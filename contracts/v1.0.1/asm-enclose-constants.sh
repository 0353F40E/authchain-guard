#!/bin/bash
perl -p -e 's/(^|\ )(?=[0-9a-fA-F]{1,21}\ )/\ <0x/g' | perl -p -e 's/(?<=\<0x[0-9a-fA-F]{1,21})(\ |$)/\>\ /g' | perl -p -e 's/^\ |\ $//g'
