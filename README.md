# Authchain Guard

Protects user's authchain output from accidental spends

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).
